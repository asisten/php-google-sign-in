<?php
if (!isset($_SESSION['token'])){
header("Location:index.php");
}
//Include Google config file
include_once 'gconfig.php';

//Unset token and user data from session
unset($_SESSION['token']);
unset($_SESSION['userData']);

//Reset OAuth access token
$gClient->revokeToken();

//Destroy entire session
session_destroy();

//Redirect to homepage
header("Location:index.php");
?>